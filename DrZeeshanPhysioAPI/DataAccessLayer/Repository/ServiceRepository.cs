﻿using DataAccessLayer.DBEntities;
using DataAccessLayer.IRepository;
using DataAccessLayer.Models;
using DrZeeshanPhysioAPI.Models;
using System.Threading.Tasks;

namespace DataAccessLayer.Repository
{
    public class ServiceRepository : IServiceRepository
    {
        private DBContext.DBContext _dbContext;

        public ServiceRepository(DBContext.DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<AppointmentsModel> ProcessAppointments(AppointmentsModel appointmentsModel)
        {
            try
            {
                var appointment = new Appointments
                {
                    FullName = appointmentsModel.FullName,
                    MobileNumber = appointmentsModel.MobileNumber,
                    AppointmentDate = appointmentsModel.AppointmentDate,
                    Address = appointmentsModel.Address,
                    Message = appointmentsModel.Message
                };
                await _dbContext.Appointments.AddAsync(appointment);
                await _dbContext.SaveChangesAsync();
                appointmentsModel.IsApointment = true;
            }
            catch
            {
                appointmentsModel.IsApointment = false;
                throw;
            }
            return appointmentsModel;
        }

        public async Task<CareersModel> ProcessCareers(CareersModel careersModel)
        {
            try
            {
                var career = new Careers
                {
                    FullName = careersModel.FullName,
                    MobileNumber = careersModel.MobileNumber,                   
                    Address = careersModel.Address,
                    Message = careersModel.Message
                };
                await _dbContext.Careers.AddAsync(career);
                await _dbContext.SaveChangesAsync();
                careersModel.IsCareer = true;
            }
            catch
            {
                careersModel.IsCareer = false;
                throw;
            }
            return careersModel;
        }
    }
}
