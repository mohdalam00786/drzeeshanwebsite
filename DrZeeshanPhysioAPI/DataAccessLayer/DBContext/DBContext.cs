﻿using DataAccessLayer.DBEntities;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.DBContext
{
    public class DBContext : DbContext
    {

        public DBContext()
        {

        }
        public DBContext(DbContextOptions<DBContext> dbContextOptions) : base(dbContextOptions)
        {

        }
        public DbSet<Appointments> Appointments { get; set; }
        public DbSet<Careers> Careers { get; set; }

    }
}
