﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DBEntities
{
    [Table("tblappointments")]
    public class Appointments
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string MobileNumber { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string Address { get; set; }
        public string Message { get; set; }
    }
}
