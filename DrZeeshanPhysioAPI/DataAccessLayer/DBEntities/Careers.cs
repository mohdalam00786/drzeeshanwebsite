﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DBEntities
{
    [Table("tblcareers")]
    public class Careers
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string MobileNumber { get; set; }       
        public string Address { get; set; }
        public string Message { get; set; }
    }
}
