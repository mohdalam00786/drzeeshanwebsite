﻿using DataAccessLayer.Models;
using DrZeeshanPhysioAPI.Models;
using System.Threading.Tasks;

namespace DataAccessLayer.IRepository
{
    public interface IServiceRepository
    {
        Task<AppointmentsModel> ProcessAppointments(AppointmentsModel appointmentsModel);
        Task<CareersModel> ProcessCareers(CareersModel careersModel);
    }
}
