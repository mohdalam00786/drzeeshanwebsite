﻿using DataAccessLayer.IRepository;
using System;

namespace DataAccessLayer.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IServiceRepository serviceRepository { get; }
    }
}
