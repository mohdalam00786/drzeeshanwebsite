﻿using DataAccessLayer.IRepository;
using DataAccessLayer.Repository;
using System;
namespace DataAccessLayer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DBContext.DBContext _dbContext;
        private IServiceRepository _serviceRepository;

        public UnitOfWork(DBContext.DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IServiceRepository serviceRepository
        {
            get
            {
                return _serviceRepository = _serviceRepository ?? new ServiceRepository(_dbContext);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
        }
    }
}
