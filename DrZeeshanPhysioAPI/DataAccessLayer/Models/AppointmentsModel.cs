﻿using System;

namespace DrZeeshanPhysioAPI.Models
{
    public class AppointmentsModel
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string MobileNumber { get; set; }
        public DateTime AppointmentDate { get; set; }
        public string Address { get; set; }
        public string Message { get; set; }
        public bool IsApointment { get; set; }
    }
}
