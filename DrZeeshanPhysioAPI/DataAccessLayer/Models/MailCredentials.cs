﻿namespace DataAccessLayer.Models
{
    public class MailCredentials
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string GmailHost { get; set; }
        public int Port { get; set; }
    }
}
