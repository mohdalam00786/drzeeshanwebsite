﻿namespace DataAccessLayer.Models
{
    public class CareersModel
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string MobileNumber { get; set; }
        public string Address { get; set; }
        public string Message { get; set; }
        public bool IsCareer { get; set; }
    }
}
