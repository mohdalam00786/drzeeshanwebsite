﻿using DataAccessLayer.IRepository;
using DataAccessLayer.Models;
using DrZeeshanPhysioAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace DrZeeshanPhysioAPI.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private IServiceRepository _serviceRepository;
        private readonly IConfiguration _config;
        public ServiceController(
           IServiceRepository serviceRepository,
           IConfiguration config
          )
        {
            _serviceRepository = serviceRepository;
            _config = config;
        }
        [HttpPost("ProcessAppointments")]
        public async Task<IActionResult> ProcessAppointments(AppointmentsModel appointmentsModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var appointments = await _serviceRepository.ProcessAppointments(appointmentsModel).ConfigureAwait(false);
                    if (appointments.IsApointment)
                    {
                        SendApointmentMail(appointmentsModel);
                    }
                    return Ok(appointments);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("ProcessCareers")]
        public async Task<IActionResult> ProcessCareers(CareersModel careersModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var careers = await _serviceRepository.ProcessCareers(careersModel).ConfigureAwait(false);
                    if (careers.IsCareer)
                    {
                        SendCareerMail(careersModel);
                    }
                    return Ok(careers);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendApointmentMail(AppointmentsModel appointmentsModel)
        {
            var mailCredentials = MailCredentials();
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress(mailCredentials.Email);
            message.To.Add(new MailAddress(mailCredentials.Email));
            message.Subject = "Patient Appointment";
            message.IsBodyHtml = false; //to make message body as html  
            message.Body = AppointmentMailBody(appointmentsModel);

            smtp.Port = mailCredentials.Port;
            smtp.Host = mailCredentials.GmailHost; //for gmail host  
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(mailCredentials.Email, mailCredentials.Password);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);
        }

        private string AppointmentMailBody(AppointmentsModel appointmentsModel)
        {
            string mailBody = "Hi Team you got a new appointment with the following details:" + "\r\n" +
                "Name: " + appointmentsModel.FullName + "\r\n" +
                               "Mobile Number: " + appointmentsModel.MobileNumber + Environment.NewLine +
                               "Appointment Date:" + appointmentsModel.AppointmentDate + Environment.NewLine +
                               "Address:" + appointmentsModel.Address + Environment.NewLine +
                               "Message:" + appointmentsModel.Message + Environment.NewLine;
            return mailBody;
        }


        private void SendCareerMail(CareersModel careersModel)
        {
            var mailCredentials = MailCredentials();
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress(mailCredentials.Email);
            message.To.Add(new MailAddress(mailCredentials.Email));
            message.Subject = "Career";
            message.IsBodyHtml = false; //to make message body as html  
            message.Body = CareerMailBody(careersModel);

            smtp.Port = mailCredentials.Port;
            smtp.Host = mailCredentials.GmailHost; //for gmail host  
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(mailCredentials.Email, mailCredentials.Password);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);
        }

        private string CareerMailBody(CareersModel careersModel)
        {
            string mailBody = "Hi Team you got a new career with the following details:" + "\r\n" +
                "Name: " + careersModel.FullName + "\r\n" +
                               "Mobile Number: " + careersModel.MobileNumber + Environment.NewLine +
                               "Address:" + careersModel.Address + Environment.NewLine +
                               "Message:" + careersModel.Message + Environment.NewLine;
            return mailBody;
        }

        private MailCredentials MailCredentials()
        {
            var mailCredentials = new MailCredentials
            {
                Email = _config.GetValue<string>("MailCredentials:Email"),
                Password = _config.GetValue<string>("MailCredentials:Password"),
                GmailHost = _config.GetValue<string>("MailCredentials:GmailHost"),
                Port = _config.GetValue<int>("MailCredentials:Port")
            };
            return mailCredentials;
        }

    }
}
