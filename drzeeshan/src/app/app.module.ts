import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/header/header.component';
import { FooterComponent } from './Components/footer/footer.component';
import { HomeComponent } from './Components/home/home.component';
import { AboutUsComponent } from './Components/about-us/about-us.component';
import { ServicesComponent } from './Components/services/services.component';
import { GalleryComponent } from './Components/gallery/gallery.component';
import { BookAppointmentComponent } from './Components/book-appointment/book-appointment.component';
import { TermsComponent } from './Components/terms/terms.component';
import { PoliciesComponent } from './Components/policies/policies.component';
import { CareerComponent } from './Components/career/career.component';
import { FaqComponent } from './Components/faq/faq.component';
import { ReviewsComponent } from './Components/reviews/reviews.component';
import { PhysiotherapyInFaridabadComponent } from './Components/physiotherapy-in-faridabad/physiotherapy-in-faridabad.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutUsComponent,
    ServicesComponent,
    GalleryComponent,
    BookAppointmentComponent,
    TermsComponent,
    PoliciesComponent,
    CareerComponent,
    FaqComponent,
    ReviewsComponent,
    PhysiotherapyInFaridabadComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
],
  exports: [
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
