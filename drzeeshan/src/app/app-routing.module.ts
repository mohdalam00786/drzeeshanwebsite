import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './Components/about-us/about-us.component';
import { BookAppointmentComponent } from './Components/book-appointment/book-appointment.component';
import { CareerComponent } from './Components/career/career.component';
import { FaqComponent } from './Components/faq/faq.component';
import { GalleryComponent } from './Components/gallery/gallery.component';
import { HomeComponent } from './Components/home/home.component';
import { PhysiotherapyInFaridabadComponent } from './Components/physiotherapy-in-faridabad/physiotherapy-in-faridabad.component';
import { PoliciesComponent } from './Components/policies/policies.component';
import { ReviewsComponent } from './Components/reviews/reviews.component';
import { ServicesComponent } from './Components/services/services.component';
import { TermsComponent } from './Components/terms/terms.component';

const routes: Routes = [
  {path:  "", pathMatch:  "full",redirectTo:  "home"},
  {path: "home", component: HomeComponent},
  {path: "about", component: AboutUsComponent},
  {path: "services", component: ServicesComponent},
  {path: "gallery", component: GalleryComponent},
  {path: "appointment", component: BookAppointmentComponent},
  {path: "terms", component: TermsComponent},
  {path: "career", component: CareerComponent},
  {path: "policies", component: PoliciesComponent},
  {path: "faqs", component: FaqComponent},
  {path: "reviews", component: ReviewsComponent},
  {path: "physiotherapy-in-faridabad", component: PhysiotherapyInFaridabadComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
