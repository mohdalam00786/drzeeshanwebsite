import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';

@Component({
  selector: 'app-physiotherapy-in-faridabad',
  templateUrl: './physiotherapy-in-faridabad.component.html',
  styleUrls: ['./physiotherapy-in-faridabad.component.scss']
})
export class PhysiotherapyInFaridabadComponent implements OnInit {

  constructor(private browserTitleService: BrowserTitleService) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Physiotherapy in Faridabad")
  }

}
