import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  constructor(private browserTitleService: BrowserTitleService) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Physiotherapy Treatment with COVID precautions in Faridabad")
  }

}
