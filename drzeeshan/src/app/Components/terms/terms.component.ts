import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  constructor(private browserTitleService: BrowserTitleService) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Re Live Physiotherapy: Terms and Conditions")
  }

}
