import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  constructor(private browserTitleService: BrowserTitleService) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "About Expert Physiotherapy in Faridabad ")
  }

}
