import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/Services/api.service';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.scss']
})
export class CareerComponent implements OnInit {
  CareerForm!: FormGroup;
  constructor(private browserTitleService: BrowserTitleService,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Join Re Live Physiotherapy")

      this.CareerForm = this.formBuilder.group(
        {
          FullName: ['', Validators.required],
          MobileNumber: ['', Validators.required],        
          Address: ['', Validators.required],
          Message: ['', Validators.required],
        }
      );
  }



  SaveCareers() {      
    this.spinner.show();
    this.apiService.SaveCareers(this.CareerForm.value).subscribe(

      (res: any) => {    
        this.spinner.hide();  
        this.toastr.success("Details save successfully", "Success")
       this.CareerForm.reset();
      },
      (error: any) => {    
        this.spinner.hide();   
        this.toastr.error("Something is wrong", "Error!!!")
        console.log(error)
      }
    )
  }
}
