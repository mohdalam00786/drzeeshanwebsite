import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/Services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr'


@Component({
  selector: 'app-book-appointment',
  templateUrl: './book-appointment.component.html',
  styleUrls: ['./book-appointment.component.scss']
})
export class BookAppointmentComponent implements OnInit {

  AppointmentForm!: FormGroup;
  constructor(private browserTitleService: BrowserTitleService,
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Book an Appointment: Physiotherapy at Home");

    this.AppointmentForm = this.formBuilder.group(
      {
        FullName: ['', Validators.required],
        MobileNumber: ['', Validators.required],
        AppointmentDate: ['', Validators.required],
        Address: ['', Validators.required],
        Message: ['', Validators.required],
      }
    );
  }

  SaveAppointments() {
    this.spinner.show();
    this.apiService.SaveAppointments(this.AppointmentForm.value).subscribe(

      (res: any) => {
        this.spinner.hide();
        this.AppointmentForm.reset();
        this.toastr.success("We will contact you shortly", "Details Saved !!")
      },
      (error: any) => {
        this.spinner.hide();
        this.toastr.error("Something is wrong", "Error")
        console.log(error)
      }
    )
  }

}
