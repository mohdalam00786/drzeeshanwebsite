import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/Services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isHideAll: boolean = true;
  IsHideBtn:boolean=false;
  
  AppointmentForm!: FormGroup;
  constructor(private browserTitleService: BrowserTitleService,
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Affordable Physiotherapy at Home")
      this.AppointmentForm = this.formBuilder.group(
        {
          FullName: ['', Validators.required],
          MobileNumber: ['', Validators.required],
          AppointmentDate: ['', Validators.required],
          Address: ['', Validators.required],
          Message: ['', Validators.required],
        }
      );
  }
  ShowAll()
  {
    this.isHideAll=false;
    this.IsHideBtn=true;
  }
 
  SaveAppointments() {
    this.spinner.show();
    this.apiService.SaveAppointments(this.AppointmentForm.value).subscribe(

      (res: any) => {
        this.spinner.hide();
        this.AppointmentForm.reset();
        this.toastr.success("We will contact you shortly", "Details Saved !!")
      },
      (error: any) => {
        this.spinner.hide();
        this.toastr.error("Something is wrong", "Error")
        console.log(error)
      }
    )
  }
}
