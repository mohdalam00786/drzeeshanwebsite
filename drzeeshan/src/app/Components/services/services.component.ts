import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  constructor(private browserTitleService: BrowserTitleService) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Physiotherapy Home Services")
  }

}
