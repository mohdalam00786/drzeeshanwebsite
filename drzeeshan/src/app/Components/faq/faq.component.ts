import { Component, OnInit } from '@angular/core';
import { BrowserTitleService } from 'src/app/Services/browser-title.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  showWhatPhysio: boolean = true;
  showWhoPhysio: boolean = false;
  showBookOnline: boolean = false;
  showVisit: boolean = false;
  showPaymentOptions: boolean = false;
  showDoctoReferral: boolean = false;
  showKids: boolean = false;
  showSession: boolean = false;
  showCertificate: boolean = false;


  constructor(private browserTitleService: BrowserTitleService) { }

  ngOnInit(): void {
    this.browserTitleService.SetBrowserTitle(
      "Re Live Physiotherapy: FAQs")
  }

  ToggleWhatPhysio() {
    this.showWhatPhysio = !this.showWhatPhysio;
  }
  ToggleWhoPhysio() {
    this.showWhoPhysio = !this.showWhoPhysio;
  }
  ToggleBookOnline() {
    this.showBookOnline = !this.showBookOnline;
  }
  ToggleVisit() {
    this.showVisit = !this.showVisit;
  }
  TogglePaymentOptions() {
    this.showPaymentOptions = !this.showPaymentOptions;
  }
  ToggleDoctoReferral()
  {
    this.showDoctoReferral=!this.showDoctoReferral;
  }
  ToggleKids()
  {
    this.showKids=!this.showKids;
  }
  ToggleSession()
  {
    this.showSession=!this.showSession;
  }
  ToggleCertificate()
  {
    this.showCertificate=!this.showCertificate;
  }
}
