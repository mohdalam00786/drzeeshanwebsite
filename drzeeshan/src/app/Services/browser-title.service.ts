import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class BrowserTitleService {

  constructor(private titleService: Title) { }
  public SetBrowserTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }
}
