import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  baseUrl:string="https://localhost:44360/api/"
  constructor(private httpClient: HttpClient,
  ) { }

  PostApiReq(Controller:any, Method:any, data:any): Observable<any> {
    return this.httpClient.post<any>(environment.baseUrl + Controller + '/' + Method, data, this.httpOptions);
  }
  SaveAppointments(data:any)
  {
    return this.PostApiReq("Service","ProcessAppointments",data);

  }

  SaveCareers(data:any)
  {
    return this.PostApiReq("Service","ProcessCareers",data);

  }
}
